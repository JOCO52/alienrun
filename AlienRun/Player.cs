﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;
using System.Collections.Generic;
using System;

namespace AlienRun
{
    class Player
    {
        // -------------------------------------
        // Data
        // -------------------------------------
        private Vector2 position = Vector2.Zero;
        private Vector2 PrevPosition = Vector2.Zero;
        private Vector2 velocity = Vector2.Zero;
        private Texture2D sprite = null;
        private Level ourLevel = null;
        private bool touchingGround = false;
        private float jumpTime = 0f;
        private bool jumpLaunchInprogress = false;
        private int Score = 0;

        // constants
        private const float MOVE_SPEED = 300.0f;
        private const float GRAVITY_ACCEL = 3400.0f;
        private const float TERMINAL_VEL = 550.0f;
        private const float JUMP_LAUCH_VEL = -1500f;
        private const float MAX_JUMP_TIME = 0.1f;
        private const float JUMO_CONTROL_POWER = 0.05f;

        // -------------------------------------
        // Behaviour
        // -------------------------------------
        public Player(Level newLevel)
        {
            ourLevel = newLevel;
        }
       //----------------------------------
        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(sprite, position, Color.White);
        }
        // -------------------------------------
        public void LoadContent(ContentManager content)
        {
            sprite = content.Load<Texture2D>("graphics/player/player-stand");
        }
        // -------------------------------------
        public void Update(GameTime gameTime)
        {
            //if we have completed level dont update the player
            if(ourLevel.GetCompletedLevel() == true)
            {
                return;
            }
            float deltaTime = (float)gameTime.ElapsedGameTime.TotalSeconds;

            // Update our velocity based on input, gravity, etc
            // Horizontal velocity is always constant because this is an auto-run game
            velocity.X = MOVE_SPEED;
            // Apply acceleration due to gravity
            velocity.Y += GRAVITY_ACCEL * deltaTime;
            // Clamp our vertical velocity to a terminal range
            velocity.Y = MathHelper.Clamp(velocity.Y, -TERMINAL_VEL, TERMINAL_VEL);

            //check if the player is jumping and apply that to velocity
            Input(gameTime);

            // pos2 = pos1 + deltaPos
            // deltaPos = velocity * deltaTime
            PrevPosition = position;
            position += velocity * deltaTime;

            //jumping mechanics
            MouseState currentState = Mouse.GetState();

            if (currentState.LeftButton == ButtonState.Pressed)
                velocity.Y = velocity.Y - (3400f + (GRAVITY_ACCEL * deltaTime));

            //check if we are colliding with anything
            CheckTileCollision();
        }
        // -------------------------------------
        private void CheckTileCollision()
        {
            //start off by assuming we are not touching the ground
            touchingGround = false;

            //use the players bounding box to get a list of tiles that they are colliding with 
            Rectangle playerBounds = getBounds();
            Rectangle PrevPlayerBounds = getPrevBounds();

            //how to get ypur list of tiles
            List<Tile> collidingtiles = ourLevel.GetTileInBounds(playerBounds);

            //for each colliding tile, move ourselves out of the tile
            foreach (Tile collidingTile in collidingtiles)
            {
                //determine how far we are overlapping the other tile
                Rectangle tileBounds = collidingTile.getBounds();
                Vector2 depth = GetCollisionDepth(tileBounds, playerBounds);
                Tile.TileType tiletype = collidingTile.GetTileType();

                //only resolve collision if there actually was one
                //depth will be Vector2.Zero if there was no collision
                if(depth != Vector2.Zero)
                {
                    if(tiletype == Tile.TileType.SPIKE)
                    {
                        Kill();
                    }

                    else if (tiletype == Tile.TileType.GOAL)
                    {
                        ourLevel.CompleteLevel();
                        return;
                    }
                    else if (tiletype == Tile.TileType.POINTS)
                    {
                        //add points to our score based on the coin type
                        //hide the coin
                        Score += collidingTile.GetPoints();

                        //hide the coin
                        collidingTile.Hide();
                        continue;
                    }
                    float absDepthX = Math.Abs(depth.X);
                    float absDepthY = Math.Abs(depth.Y);
                    

                    //resolve the collision along the shallow access
                    //we are closest to the edge of and therefore easier to squeeze out
                    // you can think of it as we only just overlapped on taht side
                    if(absDepthY < absDepthX)
                    {
                        //ew know we were falling if we were not previously overlapping from above but now we are
                        bool fallingOnToTile = playerBounds.Bottom > tileBounds.Top && PrevPlayerBounds.Bottom <= tileBounds.Top;

                        if (tiletype == Tile.TileType.IMPASSABLE || tiletype == Tile.TileType.PLATFORM & fallingOnToTile)

                        {
                            //Y is our shallow access
                            //resolve the collision along the y axis
                            position.Y += depth.Y;

                            playerBounds = getBounds();

                            //only if our feet are below the ground should we assume we are touching the ground
                            if (playerBounds.Bottom >= tileBounds.Top)
                            {
                                touchingGround = true;
                            }
                        }
                    }
                    //only handle left right collision if this tyle type is impassable
                    else if (tiletype == Tile.TileType.IMPASSABLE)
                    {
                        //X is our shallow access
                        //resolve the collision along the X axis
                        position.X += depth.X;

                        playerBounds = getBounds();
                    }
                }
            }
        }
        //--------------------------------------
        private Rectangle getBounds()
        {
            return new Rectangle((int)position.X, (int)position.Y, sprite.Width, sprite.Height);
        }

        private Rectangle getPrevBounds()
        {
            return new Rectangle((int)PrevPosition.X, (int)PrevPosition.Y, sprite.Width, sprite.Height);
        }
        //--------------------------------------
        private Vector2 GetCollisionDepth(Rectangle tile, Rectangle player)
        {
            //this Function Calculates how far our rectangles are overlapping

            //caclulate the half sizes of bothr rectangles
            float halfwidthPlayer = player.Width / 2.0f;
            float halfHeightPlayer = player.Height / 2.0f;
            float halfwidthTile = tile.Width / 2.0f;
            float halfHeightTile = tile.Height / 2.0f;

            //calculate the centers of each rectangle
            Vector2 centrePlayer = new Vector2(player.Left + halfwidthPlayer, player.Top + halfHeightPlayer);
            Vector2 centreTile = new Vector2(tile.Left + halfwidthTile, tile.Top + halfHeightTile);


            //distance between the centres
            float distanceX = centrePlayer.X - centreTile.X;
            float distanceY = centrePlayer.Y - centreTile.Y;

            //minimum distance these need to be to NOT collide/intersect
            //if either x or the Y distance is gretaer than these minima, these are not itnersecting
            float minDistanceX = halfwidthPlayer + halfwidthTile;
            float mindistanceY = halfHeightPlayer + halfHeightTile;

            //if we are not intersecting at all return (0,0)
            if (Math.Abs(distanceX) >= minDistanceX || Math.Abs(distanceY) >= mindistanceY)
            {
                return Vector2.Zero;
            }

            //calculate and return intersection depth
            //essentially how much over the minimum intersection distance are we in each direction
            //aka by how much are they intersecting in taht direction
            float depthX = 0;
            float depthY = 0;

            if (distanceX > 0)
                depthX = minDistanceX - distanceX;
            else
                depthX = -minDistanceX - distanceX;

            if (distanceY > 0)
                depthY = mindistanceY - distanceY;
            else
                depthY = -mindistanceY - distanceY;

            return new Vector2(depthX, depthY);


        }
        //--------------------------------------

        private void Input(GameTime gameTime)
        {
            KeyboardState keystate = Keyboard.GetState();
            //are we allowed to jump?
            //only true if we are touching the ground starting a jump
            //or if we are holding down the button already and havent reached our max jump time

            bool allowedToJUmp = touchingGround == true || (jumpLaunchInprogress == true && jumpTime <= MAX_JUMP_TIME);
           
            //check if the player is jumping
            if (keystate.IsKeyDown(Keys.Space) && allowedToJUmp == true)
            {
                //record that we are jumping and that jumping time has passed
                jumpLaunchInprogress = true;
                jumpTime = (float)gameTime.ElapsedGameTime.TotalSeconds;
                //we are trying to jump
                //(to simple)
                // velocity.Y = JUMP_LAUCH_VEL;

                //scale launch velocity based on how long the player holds the jump button
                //we should have a max time for the button to be held
                //thats the max laucnh velocity
                //Anything less is a fraction of max launch velocity
                velocity.Y = JUMP_LAUCH_VEL * (1.0f - (float)Math.Pow(jumpTime / MAX_JUMP_TIME, JUMO_CONTROL_POWER));
            }
            else
            {
                jumpLaunchInprogress = false;
                jumpTime = 0f;
            }
        }

        //-------------------------------------------

        public Vector2 Getposition()
        {
            return position;
        }

        private void Kill()
        {
           
                //stop everything we died
                ourLevel.SetUpLevel();
                position = Vector2.Zero;
                PrevPosition = Vector2.Zero;
                velocity = Vector2.Zero;
                jumpLaunchInprogress = false;
                Score = 0;
            
        }

        public int GetScore()
        {
            return Score;
        }
    }
}
