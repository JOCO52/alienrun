﻿using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using System.Collections.Generic;
using System;

namespace AlienRun
{
    class Level
    {
        // -------------------------------------
        // Data
        // -------------------------------------
        private Tile[,] tiles;

        private Texture2D BoxTexture;
        private Texture2D BridgeTexture;
        private Texture2D SpikeTexture;
        private Texture2D GoalTexture;
        private Texture2D GoldCoinTexture;
        private Texture2D SilverCoinTexture;
        private Texture2D BronzeCoinTexture;

        private bool completedLevel = false;

        private const int LEVEL_WIDTH = 100;
        private const int LEVEL_HEIGHT = 100;

        private const int TILE_WIDTH = 70;
        private const int TILE_HEIGT = 70;

        // -------------------------------------
        // Behaviour
        // -------------------------------------
        public void LoadContent(ContentManager content)
        {
            // Creating a single copy of the tile texture that will be used by all tiles
            BoxTexture = content.Load<Texture2D>("graphics/tiles/box");
            BridgeTexture = content.Load<Texture2D>("graphics/tiles/bridge");
            SpikeTexture = content.Load<Texture2D>("graphics/items/spikes");
            GoalTexture = content.Load<Texture2D>("graphics/tiles/goal");
            GoldCoinTexture = content.Load<Texture2D>("graphics/items/coinGold");
            SilverCoinTexture = content.Load<Texture2D>("graphics/items/coinSilver");
            BronzeCoinTexture = content.Load<Texture2D>("graphics/items/coinBronze");


            SetUpLevel();
       
            
        }

        public void SetUpLevel()
        {
            completedLevel = false;
            tiles = new Tile[LEVEL_WIDTH, LEVEL_HEIGHT];

            CreateBox(0, 8);
            CreateBox(1, 8);
            CreateBox(2, 8);
            CreateBox(3, 8);
            CreateBox(4, 8);
            CreateBox(5, 8);
            CreateBox(6, 8);
            CreateBox(7, 8);
            CreateBox(8, 8);
            CreateBox(9, 8);

            //create platforms
            CreateBridge(9, 4);
            CreateBridge(10, 4);
            CreateBridge(11, 4);
            CreateBridge(12, 4);

            //create Spikes
            CreateSpike(5, 5);

            //create goal
            CreateGoal(14, 6);

            //create coins
            CreateGoldCoin(13, 6);
            CreateSilverCoin(10, 3);
            CreateBronzeCoin(5, 7);
        }

        // -------------------------------------
        //create box
        public void CreateBox(int tileX, int tileY)
        {
            Vector2 tilePosition = new Vector2(tileX * TILE_WIDTH, tileY * TILE_WIDTH);
            Tile newTile = new Tile(BoxTexture, tilePosition, Tile.TileType.IMPASSABLE);
            tiles[tileX, tileY] = newTile;
        }
        //create bridge
        public void CreateBridge(int tileX, int tileY)
        {
            Vector2 tilePosition = new Vector2(tileX * TILE_WIDTH, tileY * TILE_WIDTH);
            Tile newTile = new Tile(BridgeTexture, tilePosition, Tile.TileType.PLATFORM);
            tiles[tileX, tileY] = newTile;
        }
        //create spike
        public void CreateSpike(int tileX, int tileY)
        {
            Vector2 tilePosition = new Vector2(tileX * TILE_WIDTH, tileY * TILE_WIDTH);
            Tile newTile = new Tile(SpikeTexture, tilePosition, Tile.TileType.SPIKE);
            tiles[tileX, tileY] = newTile;
        }
        //create goal
        public void CreateGoal(int tileX, int tileY)
        {
            Vector2 tilePosition = new Vector2(tileX * TILE_WIDTH, tileY * TILE_WIDTH);
            Tile newTile = new Tile(GoalTexture, tilePosition, Tile.TileType.GOAL);
            tiles[tileX, tileY] = newTile;
        }

        public void CreateGoldCoin(int tileX, int tileY)
        {
            Vector2 tilePosition = new Vector2(tileX * TILE_WIDTH, tileY * TILE_WIDTH);
            Tile newTile = new Tile(GoldCoinTexture, tilePosition, Tile.TileType.POINTS, 10);
            tiles[tileX, tileY] = newTile;
        }

        public void CreateSilverCoin(int tileX, int tileY)
        {
            Vector2 tilePosition = new Vector2(tileX * TILE_WIDTH, tileY * TILE_WIDTH);
            Tile newTile = new Tile(SilverCoinTexture, tilePosition, Tile.TileType.POINTS, 5);
            tiles[tileX, tileY] = newTile;
        }

        public void CreateBronzeCoin(int tileX, int tileY)
        {
            Vector2 tilePosition = new Vector2(tileX * TILE_WIDTH, tileY * TILE_WIDTH);
            Tile newTile = new Tile(BronzeCoinTexture, tilePosition, Tile.TileType.POINTS, 1);
            tiles[tileX, tileY] = newTile;
        }
        // -------------------------------------
        public void Draw(SpriteBatch spriteBatch)
        {
            for (int x = 0; x < LEVEL_WIDTH; ++x)
            {
                for (int y = 0; y < LEVEL_HEIGHT; ++y)
                {
                    //only add the tile if it exists(is not null)
                    //and is visible
                    Tile ThisTile = GetTile(x, y);
                    if (tiles[x,y] != null)
                        tiles[x, y].Draw(spriteBatch);
                }
            }
        }
        // -------------------------------------
        public List<Tile> GetTileInBounds(Rectangle bounds)
        {
            //creating an empty list to fill with tiles
            List<Tile> tilesInBounds = new List<Tile>();

            //determine the tile coordinate range for this rect
            int lefttile = (int)Math.Floor((float)bounds.Left / TILE_WIDTH);
            int righttile = (int)Math.Ceiling((float)bounds.Right / TILE_WIDTH) - 1;
            int toptile = (int)Math.Floor((float)bounds.Top / TILE_HEIGT);
            int bottomtile = (int)Math.Ceiling((float)bounds.Bottom / TILE_HEIGT) - 1;

            //loop through this range and add any tiles to the list
            for (int x = lefttile; x <= righttile; ++x)
            {
                for (int y = toptile; y <= bottomtile; ++y)
                {
                    Tile ThisTile = GetTile(x, y);

                    if (ThisTile != null && ThisTile.GetVisible() == true)
                        tilesInBounds.Add(tiles[x,y]);
                }
            }

            return tilesInBounds;
        }
        //--------------------------------------
        public Tile GetTile(int x, int y)
        {
            if (x < 0 || x >= LEVEL_WIDTH || y < 0 || y >= LEVEL_HEIGHT)
                return null;
            else
                return tiles[x, y];
        }
        //---------------------------------------------

        public void CompleteLevel()
        {
            completedLevel = true;
        }
        public bool GetCompletedLevel()
        {
            return completedLevel;
        }
    }
}
