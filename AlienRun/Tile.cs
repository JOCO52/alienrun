﻿using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace AlienRun
{
    class Tile
    {

        //--------------------------------------
        //enum
        //---------------------------------------
        public enum TileType
        {
            IMPASSABLE, // = 0 blocks player movement
            PLATFORM, // = 1 blocks player movment downard only
            SPIKE, // = 2 kills player on contact
            GOAL, //= 3  end goal of the game
            POINTS, // = 4 player gets point on contact

        }

        // -------------------------------------
        // Data
        // -------------------------------------
        private Texture2D sprite;
        private Vector2 position;
        private TileType type;
        private int points = 0;
        private bool visible = true;


        // -------------------------------------
        // Behaviour
        // -------------------------------------
        public Tile (Texture2D newSprite, Vector2 newPosition, TileType newtype, int newPoints = 0)
        {
            sprite = newSprite;
            position = newPosition;
            type = newtype;
            points = newPoints;
        }
        // -------------------------------------
        public void Draw(SpriteBatch spriteBatch)
        {
            if (visible)
            spriteBatch.Draw(sprite, position, Color.White);
        }

        public Rectangle getBounds()
        {
            return new Rectangle((int)position.X, (int)position.Y, sprite.Width, sprite.Height);
        }

        public TileType GetTileType()
        {
            return type;
        }

        public int GetPoints()
        {
            return points;
        }

        public void Hide()
        {
            visible = false;
        }

        public bool GetVisible()
        {
            return visible;
        }
    }
}
