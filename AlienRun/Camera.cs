﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace AlienRun
{
    class Camera
    {
        //data
        private Vector2 ViewSize;
        private Vector2 position;
        Player target;

        public Camera(Player newtarget, Vector2 newViewSize)
        {
            target = newtarget;
            ViewSize = newViewSize;

        }

        public void update()
        {
            position.X = target.Getposition().X - 0.5f * ViewSize.X;
        }

        public void BeginSpritebatch(SpriteBatch spriteBatch)
        {
            spriteBatch.Begin(transformMatrix: Matrix.CreateTranslation(-position.X, - position.Y, 0));
        }
    }
}
