﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace AlienRun
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class Game1 : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        SpriteBatch UIspriteBatch;

        SpriteFont Scorefont;
        SpriteFont winFont;

        Player player = null;
        Level level = null;
        Camera camera = null;

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here
            level = new Level();
            player = new Player(level);
            


            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);
            UIspriteBatch = new SpriteBatch(GraphicsDevice);
            //set screen size
            graphics.PreferredBackBufferWidth = GraphicsDevice.DisplayMode.Width;
            graphics.PreferredBackBufferHeight = GraphicsDevice.DisplayMode.Height;
            graphics.ApplyChanges();

            //create our camera
            camera = new Camera(player, new Vector2(graphics.PreferredBackBufferWidth, graphics.PreferredBackBufferHeight));

            // TODO: use this.Content to load your game content here

            player.LoadContent(Content);
            level.LoadContent(Content);
            winFont = Content.Load<SpriteFont>("fonts/largefont");
            Scorefont = Content.Load<SpriteFont>("fonts/mainfont");

        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            // TODO: Add your update logic here
            player.Update(gameTime);
            camera.update();

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            // TODO: Add your drawing code here

            //main camera and gae world draw
            camera.BeginSpritebatch(spriteBatch);

            player.Draw(spriteBatch);
            level.Draw(spriteBatch);

            spriteBatch.End();

            //UI Draw
            UIspriteBatch.Begin();

            if (level.GetCompletedLevel() == true)
            {
                Vector2 textsize = winFont.MeasureString("YOU WIN");
                Vector2 screenCentre = new Vector2(graphics.PreferredBackBufferWidth / 2, graphics.PreferredBackBufferHeight / 2);
                UIspriteBatch.DrawString(winFont, "YOU WIN!!", screenCentre - 0.5f*textsize, Color.White);
            }
            UIspriteBatch.DrawString(Scorefont, "SCORE: " + player.GetScore(), new Vector2(10, 10), Color.White);
            UIspriteBatch.End();

            

            base.Draw(gameTime);
        }
    }
}
